# Flat info

## Requirements

- docker
- docker compose

## Usage

Run `docker compose up` and the application should be accessible at `http://0.0.0.0:8080`.


## Possible improvements

- web and scraper containers currently depend on postgres being available
- plaintext password in docker compose (also hardcoded in web and scraper)
- more production ready python server (angular frontend + fast API)