
import requests
import json
import time
import psycopg2 as psy
from collections import namedtuple
import os

Data = namedtuple('data', 'name image_url')

TIMESTAMP = time.time_ns() // 1000 
PAGE_SIZE = 500
URL = f'https://www.sreality.cz/api/en/v2/estates?category_main_cb=1&category_type_cb=1&per_page={PAGE_SIZE}&tms={TIMESTAMP}&sort=0'

HOST = os.environ.get('POSTGRES_HOST', 'localhost')

PG_CONN_SETTINGS = {
        'dbname': 'postgres',
        'user': 'postgres',
        'password': 'postgres',
        'port': 5432,
        'host': HOST
    }

def extract_data(item):
    name = item['name']
    image = item['_links']['images'][0]['href']

    return Data(name, image)

def create_table(conn):    
    with conn.cursor() as cursor:
        try:
            cursor.execute("""
                CREATE TABLE IF NOT EXISTS SITE_DATA (
                    id SERIAL PRIMARY KEY,
                    title VARCHAR(255) NOT NULL,
                    image_url VARCHAR(255) NOT NULL
                )
                """)

        except (Exception, psy.DatabaseError) as error:
            print('Error creating table')
            raise error

def save_data(conn, data: [Data]):
    with conn.cursor() as cursor:
        try:
            cursor.executemany("""
                INSERT INTO SITE_DATA (title, image_url) VALUES (%s,%s)
            """, data)
        except (Exception, psy.DatabaseError) as error:
            print('Error creating data')
            raise error    

if __name__ == '__main__':
    print("Started scraping estates")

    response = requests.get(URL)
    data = response.json()

    estates = data['_embedded']['estates']
    processed = list(map(extract_data, estates))

    conn = psy.connect(**PG_CONN_SETTINGS)
    conn.autocommit = True

    create_table(conn)
    save_data(conn, processed)

    conn.close()

    print("Successfully scraped estates")
