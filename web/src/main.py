from flask import Flask, render_template
from waitress import serve
from collections import namedtuple
import psycopg2 as psy
from psycopg2.pool import SimpleConnectionPool
import os
import logging

Data = namedtuple('data', 'name image_url')

LOGGER = logging.getLogger('waitress')
LOGGER.setLevel(level='INFO')

HOST = os.environ.get('POSTGRES_HOST', 'localhost')

PG_CONN_SETTINGS = {
        'dbname': 'postgres',
        'user': 'postgres',
        'password': 'postgres',
        'port': 5432,
        'host': HOST
    }

pg_pool = SimpleConnectionPool(1, 10, **PG_CONN_SETTINGS)

app = Flask(__name__)

@app.route('/')
def display_data():
   site_data = get_data()
   return render_template('data.html.jinja', data=site_data)

def get_data():
   with pg_pool.getconn() as conn:
      with conn.cursor() as cursor:
         try:
            cursor.execute("SELECT title, image_url FROM SITE_DATA")
            flats = cursor.fetchall()
            return list(map(lambda x: Data(x[0], x[1]), flats))
         except (Exception, psy.DatabaseError) as error:
            LOGGER.error('Error querying data')
            raise error    


if __name__ == "__main__":
   serve(app, host="0.0.0.0", port=8080)